#ifndef _AUTO_KEY_CHECK_H
#define _AUTO_KEY_CHECK_H

void auto_key_check_press();
void auto_key_input_code(bool s);


void auto_key_check_press()
{
	bool key_di = false;
	bool key_da = false;
	// 检查按键是否被按下
	if(digitalRead(pin_key_di) == 0) {
		// auto_key_input_code(s_auto_key_type);
		key_di = true;
	}
	if(digitalRead(pin_key_da) == 0) {
		// auto_key_input_code(!s_auto_key_type);
		key_da = true;
	}
	if(key_di) {
		auto_key_input_code(s_auto_key_type);
	}
	if(key_da) {
		auto_key_input_code(!s_auto_key_type);
	}
	if(digitalRead(pin_key_di) == 1 && digitalRead(pin_key_da) == 1 && flag_letter == 0) {
		cs_time = millis();
		flag_letter = 1;
	}
}

// s = true  键入di
// s = false 键入da
void auto_key_input_code(bool s) {
	flag_letter = 0;
	flag_space = 0;
	// 电码间隔
	int sp_time = u_time / 2;
	// 蜂鸣器鸣叫时长
	int bee_on_time;
	if(s) {
		key_code += ".";
		d_text_code += ".";
		Serial.print(".");
		bee_on_time = u_time / 2;
		send_code += ".";
		
	}
	else  {
		key_code += "-";
		d_text_code += "-";
		Serial.print("-");
		bee_on_time = u_time * 1.5;
		send_code += "-";
	}

	bee(true);
	tt(true);
	d_home_ref();
	delay(bee_on_time - tt(false));
	bee(false);
	delay(sp_time);

}


#endif