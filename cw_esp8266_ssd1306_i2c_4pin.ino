// 用户参数设置

// 呼号
String callsign = "ZClub";

// 主路由器SSID
String ssid_1 = "***";
// 主路由器密码
String password_1 = "***";

// 备用路由器SSID
String ssid_2 = "***";
// 备用路由器密码
String password_2 = "***";

// MQTT服务器
const char *mqtt_server = "cw.funner.pub";
// MQTT服务器端口
const int port = 1128;
// TOPIC
const char *topic_name = "cw";



// 引脚定义
#include "pin.h"
// 设置参数
#include "var_settings.h"
// 莫尔斯电码
#include "var_morse.h"
// 时间操作工具
#include "time_tools.h"
// 蜂鸣器操作
#include "op_bee.h"
// 显示
#include "display.h"
// 电码处理
#include "op_code.h"
// 网络连接
#include "wifi.h"
// MQTT连接
#include "mqtt.h"
// 存储设置
#include "op_settings.h"
// 编码器操作
#include "op_rot.h"
// 串口显示
#include "op_ser.h"
// 手动电键
#include "key_check.h"
// 自动电键
#include "auto_key_check.h"


void setup()
{
	// 初始化引脚
	pinMode(pin_bee, OUTPUT);
	pinMode(pin_key, INPUT_PULLUP);
	pinMode(pin_sw, INPUT_PULLUP);
	pinMode(pin_dt, INPUT_PULLUP);
	pinMode(pin_clk, INPUT_PULLUP);

	// 自动键引脚
	pinMode(pin_key_di, INPUT_PULLUP);
	pinMode(pin_key_da, INPUT_PULLUP);

	// 初始化串口
	Serial.begin(115200);
	Serial.println();

	// 生成client id
	create_client_id();

	// 初始化显示屏
	d_init();

	// 初始化EEPROM
	eeprom_init();

	// 检查是否是首次运行代码
	if(check_first_run()) {
		// 将设置置为默认
		settings_default();
	}
	else {
		// 读取设置
		get_settings("all");
	}
}

void loop()
{
	if (flag_net)
	{
		if (!client.connected() && flag_net)
		{
			if (WiFi.status() != WL_CONNECTED)
			{
				init_wifi();
			}
			if (!flag_mqtt_init) {
				mqtt_init();
			}
			reconnect();
		}
		client.loop();
	}
	// 检查字符是否输入完成，停止输入的时间超过一个单位时间的1.2倍则代表此次字符输入完成，将输入的内容进行识别。
	check_letter();

	// 检查按键
	// 手动建
	if (s_key_type)
	{
		// 手动按键
		check_key_press();
		// 检查按键是否释放
		check_key_release();
	}
	// 自动键
	else
	{
		auto_key_check_press();
	}
	// 检测串口输入的数据
	check_serial_input();

	// 检查是否要刷新屏幕
	if (flag_d_ref == 1)
	{
		flag_d_ref = 0;
		d_home_ref();
	}
	// 检查电位器是否被按动
	check_btn();

	// 检查发送电码
	if (flag_send == 1)
	{
		// 如果关闭了网络，则什么都不做，并且将发送代码标识置为否。
		if(flag_net == false) {
			flag_send = 0;
		}
		// 如果没有关闭网络，则检查延时是否到了要发送的时间，如果到了，则发送电码
		else {
			int diff_time = millis() - cs_time;
			if (diff_time > 1500 && send_code != "")
			{
				flag_send = 0;
				send_code_to_server();
			}
		}
	}
}






